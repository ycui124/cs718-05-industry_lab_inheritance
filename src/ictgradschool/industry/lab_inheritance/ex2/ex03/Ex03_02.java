package ictgradschool.industry.lab_inheritance.ex2.ex03;

public class Ex03_02 {
    public static void main(String[] args) {
        SuperClass s2 = new Test1();
        System.out.println("\nThe static Binding");
        System.out.println("S2.x = " + s2.x);
        System.out.println("S2.y = " + s2.y);
        System.out.println("S2.foo() = " + s2.foo());
        System.out.println("S2.goo2() = " + s2.goo());
    }

}
