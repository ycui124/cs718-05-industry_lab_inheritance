package ictgradschool.industry.lab_inheritance.ex2.ex03;

public class Test2 extends SuperClass{
    static int x = 15;
    static int y = 15;
    int x2= 20;
    static int y2 = 20;
    Test2() {
        x2 = y2++;
    }
    public int foo2() {
        return x2;
    }
    public static int goo2() {
        return y2;
    }
    public static int goo(){
        return y2;
    }
}
