package ictgradschool.industry.lab_inheritance.ex2.ex03;

public class Test1 extends SuperClass{
    int x2= 20;
    static int y2 = 20;
    Test1() {
        x2 = y2++;
    }
    public int foo2() {
        return x2;
    }
    public static int goo2() {
        return y2;
    }
}
